export async function revokeApiKey() {
  const res = await fetch("/api/api-key/revoke", {
    method: "GET",
  });
  const data = (await res.json()) as { error?: string };

  if (data.error) {
    throw new Error(data.error);
  }
}
